doAsyncThing.then(function(result){
	return doAnotherAsyncThing().then(function(result){
		return doYetAnotherAsyncThing().then(function(result){
			console.log("Done!");
		});
	});
}).catch(function(err){
	/* This is where errors from *any* of the above promises end up. */
});

/* ... is functionally the same as ... */

Promise.try(function(){
	return doAsyncThing();
}).then(function(result){
	return doAnotherAsyncThing();
}).then(function(result){
	return doYetAnotherAsyncThing();
}).then(function(result){
	console.log("Done!");
}).catch(function(err){
	/* This is where errors from *any* of the above promises end up. */
});